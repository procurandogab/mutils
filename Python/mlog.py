#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""
SSCA (Self-Security-Control Archticture) auto build all projects process

Copyright (c), GreatWall Group Co,.Ltd. All rights reserved.<BR>

This software and associated documentation (if any) is furnished
under a license and may only be used or copied in accordance
with the terms of the license. Except as permitted by such
license, no part of this software or documentation may be
reproduced, stored in a retrieval system, or transmitted in any
form or by any means without the express written consent of
Great Wall Group Co.,Ltd.
"""
# 作者: jiangweixi
# 时间: 2022/11/10 下午7:30
# 描述: 示例三: 自定义logging字典

import logging.config
from datetime import datetime
from pathlib import Path

__all__ = [
    "debug_flag",
    "get_logger"
]

# 是否以debug模式打印(控制台输出全部日志)
debug_flag = False


# """
# 日志等级：使用范围
#
# FATAL：致命错误
# CRITICAL：特别糟糕的事情，如内存耗尽、磁盘空间为空，一般很少使用
# ERROR：发生错误时，如IO操作失败或者连接问题
# WARNING：发生很重要的事件，但是并不是错误时，如用户登录密码错误
# INFO：处理请求或者状态变化等日常事务
# DEBUG：调试过程中使用DEBUG等级，如算法中每个循环的中间状态
# """
# """
# fomatter中可用变量
# %(levelno)s: 打印日志级别的数值
# %(levelname)s: 打印日志级别名称
# %(pathname)s: 打印当前执行程序的路径，其实就是sys.argv[0]
# %(module)s: 打印当前模块名称
# %(filename)s: 打印当前执行程序名
# %(funcName)s: 打印日志的当前函数
# %(lineno)d: 打印日志的当前行号
# %(asctime)s: 打印日志的时间
# %(thread)d: 打印线程ID
# %(threadName)s: 打印线程名称
# %(process)d: 打印进程ID
# %(message)s: 打印日志信息
# """

# 给过滤器使用的判断
class RequireDebugTrue(logging.Filter):
    # 实现filter方法
    def filter(self, record):
        if not debug_flag and record.levelno == 10:  # 以debug模式打印
            return False
        return True


# dtime = str(datetime.now())[:16] \
#         .replace("-", "_") \
#         .replace(" ", "_") \
#         .replace(":", "_")
dtime = "0"
log_dir = Path(__file__).parent.joinpath('logs')
fulllog_filepath = log_dir.joinpath(f"full-log_{dtime}.log")
framework_log_filepath = log_dir.joinpath(f"ci-framework_{dtime}.log")
cases_log_filepath = log_dir.joinpath(f"ci-cases_{dtime}.log")
result_log_filepath = log_dir.joinpath(f"ci-results.log")
logging_config = {
    # 必选项，其值是一个整数值，表示配置格式的版本，当前唯一可用的值就是1
    'version': 1,
    # 是否禁用现有的记录器
    'disable_existing_loggers': False,

    # 过滤器
    'filters': {
        'require_debug_true': {
            # 在开发环境，设置DEBUG为True；
            # 在客户端，设置DEBUG为False。从而控制是否需要使用某些处理器。
            '()': RequireDebugTrue,
        }
    },

    # 日志格式集合
    'formatters': {
        'simple': {
            'format': '%(asctime)s [%(levelname)s] - %(message)s',
            'datefmt': "%H:%M:%S",
        },
        'simple2': {
            'format': '%(asctime)s [%(levelname)s] [%(name)s] - %(message)s',
            'datefmt': "%H:%M:%S",
        },
        'case-result': {
            'format': '%(asctime)s [%(levelname)s] [用例结果] - %(message)s',
            'datefmt': "%H:%M:%S",
        },
        "standard": {
            'format': '%(asctime)s [%(levelname)s] '
                      '[%(filename)s::%(funcName)s::%(lineno)d] - %(message)s'
        },
        "standard-full": {
            'format': '%(asctime)s [%(levelname)s] [%(name)s] '
                      '[%(filename)s::%(funcName)s::%(lineno)d] - %(message)s'
        },
    },

    # 处理器集合
    'handlers': {
        # INFO输出到控制台 - level gt info
        'console-info': {
            'level': 'INFO',  # 输出信息的最低级别
            'class': 'logging.StreamHandler',
            "stream": "ext://sys.stdout",
            'formatter': 'simple2',  # 使用simple2格式
            'filters': ['require_debug_true', ]
        },
        # WARN输出到控制台 - level gt warning
        'console-warn': {
            'level': 'WARNING',  # 输出信息的最低级别
            'class': 'logging.StreamHandler',
            "stream": "ext://sys.stdout",
            'formatter': 'simple2',  # 使用simple2格式
            'filters': ['require_debug_true', ]
        },
        # 用例结果输出到控制台 - level gt info
        'console-caseresult': {
            'level': 'INFO',  # 输出信息的最低级别
            'class': 'logging.StreamHandler',
            "stream": "ext://sys.stdout",
            'formatter': 'case-result',  # 使用case-result格式
            'filters': ['require_debug_true', ]
        },
        # 输出到完整日志文件
        'logfile_full': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'standard-full',
            'filename': fulllog_filepath,  # 输出位置
            'maxBytes': 1024 * 1024 * 5,  # 文件大小 5M
            'backupCount': 5,  # 备份份数
            'encoding': 'utf8',  # 文件编码
        },
        # 输出到框架日志文件
        'logfile_framework': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'standard',
            'filename': framework_log_filepath,  # 输出位置
            'maxBytes': 1024 * 1024 * 5,  # 文件大小 5M
            'backupCount': 5,  # 备份份数
            'encoding': 'utf8',  # 文件编码
        },
        # 输出到用例日志文件
        'logfile_cases': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'standard',
            'filename': cases_log_filepath,  # 输出位置
            'maxBytes': 1024 * 1024 * 5,  # 文件大小 5M
            'backupCount': 5,  # 备份份数
            'encoding': 'utf8',  # 文件编码
        },
        # 用例结果输出到文件 - level gt info
        'logfile-caseresult': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'case-result',
            'filename': result_log_filepath,  # 输出位置
            'maxBytes': 1024 * 1024 * 5,  # 文件大小 5M
            'backupCount': 5,  # 备份份数
            'encoding': 'utf8',  # 文件编码
        },
    },

    # 日志管理器集合
    'loggers': {
        'root': {           # Python 3.8.10,
            'handlers': ['logfile_full'],
            'level': 'DEBUG',
            'propagate': False,  # root就是根记录器
        },
        '框架日志': {
            'handlers': ['console-warn', 'logfile_framework', 'logfile_full'],
            'level': 'DEBUG',
            'propagate': False,  # 是否传递给父记录器,
        },
        '用例日志': {
            'handlers': ['console-info', 'logfile_cases', 'logfile_full'],
            'level': 'DEBUG',
            'propagate': False,  # 是否传递给父记录器,
        },
        '用例结果': {
            'handlers': ['console-caseresult',
                         'logfile-caseresult',
                         'logfile_full'],
            'level': 'DEBUG',
            'propagate': False,  # 是否传递给父记录器,
        },

    }
}

logging.config.dictConfig(logging_config)


# 获取自定义logger. 调用时logger等价于logging，使用两者都可
def get_logger(name='root'):
    return logging.getLogger(name)


if __name__ == "__main__":
    logger = get_logger("框架日志")

    # 关闭终端控制台debug模式输出
    debug_flag = False

    logger.debug('Python debug')
    logger.info('Python info')
    logger.warning('Python warning')
    logger.error('Python Error')
    logger.critical('Python critical')


